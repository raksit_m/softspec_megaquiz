package com.example.softspec.megaquiz.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.softspec.megaquiz.R;
import com.example.softspec.megaquiz.models.Answer;
import com.example.softspec.megaquiz.models.Choice;
import com.example.softspec.megaquiz.models.Quiz;
import com.example.softspec.megaquiz.models.Storage;
import com.example.softspec.megaquiz.models.User;

import org.eazegraph.lib.charts.PieChart;
import org.eazegraph.lib.models.PieModel;

import java.util.Observable;
import java.util.Observer;

public class QuizViewActivity extends AppCompatActivity implements Observer {

    public static final int NUM_CHOICES = 4;
    private TextView tv_question;
    private Button btn_choice1;
    private Button btn_choice2;
    private Button btn_choice3;
    private Button btn_choice4;
    private CardView answerLayout;
    private Choice[] choices;
    private Button[] buttons;
    private int[] colors;
    private PieChart pieChart;

    private Quiz quiz;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initComponents();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Storage.getInstance().deleteObservers();
        Storage.getInstance().addObserver(this);
    }

    private void initComponents() {
        tv_question = (TextView) findViewById(R.id.tv_question);
        btn_choice1 = (Button) findViewById(R.id.btn_choice1);
        btn_choice2 = (Button) findViewById(R.id.btn_choice2);
        btn_choice3 = (Button) findViewById(R.id.btn_choice3);
        btn_choice4 = (Button) findViewById(R.id.btn_choice4);

        answerLayout = (CardView) findViewById(R.id.layout_answers);
        answerLayout.setAlpha(0f);
        answerLayout.setTranslationY(100);

        quiz = (Quiz) getIntent().getSerializableExtra("quiz");
        user = (User) getIntent().getSerializableExtra("user");
        User creator = (User) getIntent().getSerializableExtra("creator");

        tv_question.setText(quiz.getQuestion());

        choices = quiz.getChoices();
        btn_choice1.setText(choices[0].getDescription());
        btn_choice2.setText(choices[1].getDescription());
        btn_choice3.setText(choices[2].getDescription());
        btn_choice4.setText(choices[3].getDescription());

        this.setTitle("Quiz ID: " + quiz.getId());

        Storage.getInstance().getAnswerFromQuizAndUserID(quiz.getId(), user.getUserID());

        buttons = new Button[NUM_CHOICES];
        buttons[0] = btn_choice1;
        buttons[1] = btn_choice2;
        buttons[2] = btn_choice3;
        buttons[3] = btn_choice4;

        for (int i = 0; i < NUM_CHOICES; i++) {
            final int temp = i;
            buttons[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int quizID = quiz.getId();
                    int choiceID = choices[temp].getId();
                    String userID = user.getUserID();
                    Storage.getInstance().addAnswer(quizID, choiceID, userID);
                    Storage.getInstance().getAllData();
                    preventAnsweringAgain(temp + 1);
                }
            });
        }

        colors = new int[NUM_CHOICES];
        colors[0] = Color.parseColor("#FF4081");
        colors[1] = Color.parseColor("#2196F3");
        colors[2] = Color.parseColor("#8D6E63");
        colors[3] = Color.parseColor("#FFEB3B");

        pieChart = (PieChart) findViewById(R.id.pie_chart);

        if (creator.getUserID().equals(user.getUserID())) {
            preventAnsweringAgain();
            Storage.getInstance().getAnswersFromQuizID(quiz.getId());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void preventAnsweringAgain() {
        for (int i = 0; i < buttons.length; i++) {
            buttons[i].setEnabled(false);
            buttons[i].setTextColor(ContextCompat.getColor(this, R.color.grey_400));
            ViewCompat.setBackgroundTintList(buttons[i], ContextCompat.getColorStateList(this, R.color.grey_300));
        }
    }

    private void preventAnsweringAgain(int answerIndex) {
        for (int i = 0; i < buttons.length; i++) {
            if (i == answerIndex - 1) {
                buttons[i].setClickable(false);
            } else {
                buttons[i].setEnabled(false);
                buttons[i].setTextColor(ContextCompat.getColor(this, R.color.grey_400));
                ViewCompat.setBackgroundTintList(buttons[i], ContextCompat.getColorStateList(this, R.color.grey_300));
            }
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        if (data instanceof Answer) {
            Answer answer = (Answer) data;
            if (answer != null) {
                int answerIndex = answer.getChoice().getId() % NUM_CHOICES;
                if(answerIndex == 0) {
                    answerIndex = 4;
                }

                preventAnsweringAgain(answerIndex);
                Storage.getInstance().getAnswersFromQuizID(quiz.getId());
            }
        } else if (data instanceof int[]) {
            int[] numAnswersArray = (int[]) data;
            int sum = 0;

            for (int i = 0; i < numAnswersArray.length; i++) {
                pieChart.addPieSlice(new PieModel(choices[i].getDescription(), numAnswersArray[i], colors[i]));
                sum += numAnswersArray[i];
            }

            if (sum == 0) pieChart.clearChart();

            answerLayout.animate().alpha(1f).translationY(0).setDuration(300);
            pieChart.startAnimation();
        }
    }
}