package com.example.softspec.megaquiz.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.softspec.megaquiz.R;
import com.example.softspec.megaquiz.models.Storage;
import com.example.softspec.megaquiz.models.User;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Earth on 17/4/2559.
 */
public class UserAdapter extends ArrayAdapter<User> {

    public UserAdapter(Context context, int resource, List<User> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if(v == null) {
            LayoutInflater vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.user_cell, null);
        }

        if(v != null) {
            TextView tv_user_name = (TextView) v.findViewById(R.id.tv_user_name);
            TextView tv_information = (TextView) v.findViewById(R.id.tv_information);
            ImageView imgv_profile_picture = (ImageView) v.findViewById(R.id.imgv_profile_picture);
            User user = getItem(position);
            tv_user_name.setText(user.getProfileName());
            int numQuizzes = Storage.getInstance().getQuizCreatedTimesFromUserID(user.getUserID());
            int numAnswers = Storage.getInstance().getAnswerTimesFromUserID(user.getUserID());
            tv_information.setText(String.format("%d Quizzes, %d Answers", numQuizzes, numAnswers));
            Picasso.with(getContext()).load(user.getProfilePictureUri()).into(imgv_profile_picture);
        }

        return v;
    }
}
