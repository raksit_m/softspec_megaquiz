package com.example.softspec.megaquiz.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.softspec.megaquiz.R;
import com.example.softspec.megaquiz.models.Quiz;
import com.example.softspec.megaquiz.models.Storage;
import com.example.softspec.megaquiz.models.User;
import com.example.softspec.megaquiz.views.QuizAdapter;
import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.nineoldandroids.view.ViewHelper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class ProfileActivity extends AppCompatActivity implements Observer {

    private RecyclerView recyclerView;
    private Toolbar toolbar;
    private List<Quiz> quizzes;
    private QuizAdapter quizAdapter;
    private TextView tv_profile_name;
    private TextView tv_num_quizzes;
    private TextView tv_num_answers;
    private ImageView imgv_profile_picture;
    private SwipeRefreshLayout swipeRefreshLayout;

    private User creator;
    private User viewer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initComponents();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Storage.getInstance().deleteObservers();
        Storage.getInstance().addObserver(this);
    }

    private void initComponents() {
        quizzes = new ArrayList<Quiz>();
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        quizAdapter = new QuizAdapter(quizzes);
        viewer = (User) getIntent().getSerializableExtra("viewer");
        creator = (User) getIntent().getSerializableExtra("creator");
        Storage.getInstance().getAllData();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(quizAdapter);
        quizAdapter.setOnItemClickListener(new QuizAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(ProfileActivity.this, QuizViewActivity.class);
                Quiz quiz = quizzes.get(position);
                intent.putExtra("quiz", quiz);
                intent.putExtra("user", viewer);
                intent.putExtra("creator", creator);
                startActivity(intent);
            }
        });

        tv_profile_name = (TextView) findViewById(R.id.tv_profile_name);
        tv_profile_name.setText(creator.getProfileName());
        imgv_profile_picture = (ImageView) findViewById(R.id.imgv_profile_picture);
        Picasso.with(this).load(creator.getProfilePictureUri()).into(imgv_profile_picture);
        tv_num_quizzes = (TextView) findViewById(R.id.tv_num_quizzes);
        tv_num_answers = (TextView) findViewById(R.id.tv_num_answers);
        this.setTitle(creator.getProfileName());

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                recyclerView.setVisibility(View.GONE);
                Storage.getInstance().getAllData();
            }
        });
    }

    private void refreshQuizzes() {
        quizzes.clear();
        for (Quiz q : Storage.getInstance().getQuizzes()) {
            if(q.getUserID().equals(creator.getUserID()))
            quizzes.add(q);
        }
        quizAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == android.R.id.home) {
            Intent intent = new Intent(ProfileActivity.this, UserActivity.class);
            intent.putExtra("user_id", viewer.getUserID());
            intent.putExtra("user_profile_name", viewer.getProfileName());
            intent.putExtra("user_profile_picture_uri", viewer.getProfilePictureUri());
            startActivity(intent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void update(Observable observable, Object data) {
        refreshQuizzes();
        int num_quizzes = Storage.getInstance().getQuizCreatedTimesFromUserID(creator.getUserID());
        tv_num_quizzes.setText(String.format("%d", num_quizzes));
        int num_answers = Storage.getInstance().getAnswerTimesFromUserID(creator.getUserID());
        tv_num_answers.setText(String.format("%d", num_answers));
        recyclerView.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setRefreshing(false);
    }
}
