package com.example.softspec.megaquiz.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.softspec.megaquiz.R;
import com.example.softspec.megaquiz.models.Storage;
import com.example.softspec.megaquiz.models.User;
import com.example.softspec.megaquiz.views.UserAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class UserActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, Observer {

    private ListView listView;
    private List<User> users;
    private UserAdapter userAdapter;

    private NavigationView navigationView;
    private ImageView imgv_profile_picture;
    private TextView tv_profile_name;
    private ProgressBar progressBar;
    private SearchView searchView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private String userID;
    private String userProfileName;
    private String userProfilePictureUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        initComponents();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Storage.getInstance().deleteObservers();
        Storage.getInstance().addObserver(this);
    }

    private void initComponents() {
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        users = new ArrayList<User>();
        Storage.getInstance().getAllData();
        userAdapter = new UserAdapter(this, R.layout.user_cell, users);
        listView = (ListView) findViewById(R.id.list_view);
        listView.setVisibility(View.GONE);
        listView.setAdapter(userAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(UserActivity.this, ProfileActivity.class);
                intent.putExtra("creator", users.get(position));
                intent.putExtra("viewer", Storage.getInstance().findUserById(userID));
                startActivity(intent);
            }
        });

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                listView.setVisibility(View.GONE);
                Storage.getInstance().getAllData();
            }
        });
        setNavigationView();
    }

    private void refreshUsers() {
        users.clear();
        for(User u : Storage.getInstance().getUsers()) {
            users.add(u);
        }
        userAdapter.notifyDataSetChanged();
    }

    private void setNavigationView() {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);

        imgv_profile_picture = (ImageView) headerView.findViewById(R.id.imgv_profile_picture);
        tv_profile_name = (TextView) headerView.findViewById(R.id.tv_profile_name);

        userID = getIntent().getStringExtra("user_id");
        userProfileName = getIntent().getStringExtra("user_profile_name");
        userProfilePictureUri = getIntent().getStringExtra("user_profile_picture_uri");
        tv_profile_name.setText(userProfileName);
        Picasso.with(this).load(userProfilePictureUri).into(imgv_profile_picture);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.user, menu);
        MenuItem menuItem = menu.findItem(R.id.menu_search);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menuItem.getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        setupSearchView();
        return true;
    }

    private void setupSearchView() {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                users.clear();
                for(User u : Storage.getInstance().getUsers()) {
                    if(u.getProfileName().toLowerCase().contains(newText.toLowerCase())) {
                        users.add(u);
                    }
                }
                userAdapter.notifyDataSetChanged();
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_all_quizzes) {
            Intent intent = new Intent(UserActivity.this, MainActivity.class);
            intent.putExtra("user_id", userID);
            intent.putExtra("user_profile_name", userProfileName);
            intent.putExtra("user_profile_picture_uri", userProfilePictureUri);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_my_quiz) {
            Intent intent = new Intent(UserActivity.this, MyQuizActivity.class);
            intent.putExtra("user_id", userID);
            intent.putExtra("user_profile_name", userProfileName);
            intent.putExtra("user_profile_picture_uri", userProfilePictureUri);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_users) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void update(Observable observable, Object data) {
        refreshUsers();
        progressBar.setVisibility(View.GONE);
        listView.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setRefreshing(false);
    }
}
