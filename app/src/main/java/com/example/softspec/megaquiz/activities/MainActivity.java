package com.example.softspec.megaquiz.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.softspec.megaquiz.R;
import com.example.softspec.megaquiz.models.Quiz;
import com.example.softspec.megaquiz.models.Storage;
import com.example.softspec.megaquiz.models.User;
import com.example.softspec.megaquiz.views.QuizAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, Observer {

    private RecyclerView recyclerView;
    private List<Quiz> quizzes;
    private QuizAdapter quizAdapter;

    private NavigationView navigationView;
    private ImageView imgv_profile_picture;
    private TextView tv_profile_name;
    private ProgressBar progressBar;
    private SwipeRefreshLayout swipeRefreshLayout;
    private SearchView searchView;
    private String userID;
    private String userProfileName;
    private String userProfilePictureUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        initComponents();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Storage.getInstance().deleteObservers();
        Storage.getInstance().addObserver(this);
    }

    private void initComponents() {
        quizzes = new ArrayList<Quiz>();
        quizAdapter = new QuizAdapter(quizzes);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        Storage.getInstance().getAllData();
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setVisibility(View.GONE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(quizAdapter);
        quizAdapter.setOnItemClickListener(new QuizAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(MainActivity.this, QuizViewActivity.class);
                Quiz quiz = quizzes.get(position);
                User user = Storage.getInstance().findUserById(userID);
                User creator = Storage.getInstance().findUserById(quiz.getUserID());
                intent.putExtra("quiz", quiz);
                intent.putExtra("user", user);
                intent.putExtra("creator", creator);
                startActivity(intent);

            }
        });

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                recyclerView.setVisibility(View.GONE);
                Storage.getInstance().getAllData();
            }
        });

        setNavigationView();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.btn_new_quiz);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NewQuizActivity.class);
                intent.putExtra("user_id", userID);
                startActivity(intent);
            }
        });
    }

    private void refreshQuizzes() {
        quizzes.clear();
        Log.e("ACTIVITY", Storage.getInstance().getQuizzes().size() + "");
        for (Quiz q : Storage.getInstance().getQuizzes()) {
            quizzes.add(q);
        }
        quizAdapter.notifyDataSetChanged();
    }

    private void setNavigationView() {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);

        imgv_profile_picture = (ImageView) headerView.findViewById(R.id.imgv_profile_picture);
        tv_profile_name = (TextView) headerView.findViewById(R.id.tv_profile_name);

        userID = getIntent().getStringExtra("user_id");
        userProfileName = getIntent().getStringExtra("user_profile_name");
        userProfilePictureUri = getIntent().getStringExtra("user_profile_picture_uri");
        tv_profile_name.setText(userProfileName);
        Picasso.with(this).load(userProfilePictureUri).into(imgv_profile_picture);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem searchItem = menu.findItem(R.id.menu_search);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) searchItem.getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        setupSearchView();
        return true;
    }

    private void setupSearchView() {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                quizzes.clear();
                for (Quiz q : Storage.getInstance().getQuizzes()) {
                    if (q.getQuestion().toLowerCase().contains(newText.toLowerCase())) {
                        quizzes.add(q);
                    }
                }
                quizAdapter.notifyDataSetChanged();
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_all_quizzes) {

        } else if (id == R.id.nav_my_quiz) {
            Intent intent = new Intent(MainActivity.this, MyQuizActivity.class);
            intent.putExtra("user_id", userID);
            intent.putExtra("user_profile_name", userProfileName);
            intent.putExtra("user_profile_picture_uri", userProfilePictureUri);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_users) {
            Intent intent = new Intent(MainActivity.this, UserActivity.class);
            intent.putExtra("user_id", userID);
            intent.putExtra("user_profile_name", userProfileName);
            intent.putExtra("user_profile_picture_uri", userProfilePictureUri);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void update(Observable observable, Object data) {
        refreshQuizzes();
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setRefreshing(false);
    }
}