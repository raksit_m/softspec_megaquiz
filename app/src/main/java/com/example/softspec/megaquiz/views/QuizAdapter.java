package com.example.softspec.megaquiz.views;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.softspec.megaquiz.R;
import com.example.softspec.megaquiz.models.Quiz;
import com.example.softspec.megaquiz.models.Storage;
import com.example.softspec.megaquiz.models.User;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Earth on 7/4/2559.
 */
public class QuizAdapter extends RecyclerView.Adapter<QuizAdapter.QuizViewHolder> {

    private List<Quiz> quizzes;
    OnItemClickListener itemClickListener;

    public class QuizViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_user_name;
        TextView tv_question;
        ImageView imgv_profile_picture;

        QuizViewHolder(View view) {
            super(view);
            tv_user_name = (TextView) view.findViewById(R.id.tv_user_name);
            tv_question = (TextView) view.findViewById(R.id.tv_question);
            imgv_profile_picture = (ImageView) view.findViewById(R.id.imgv_profile_picture);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(itemClickListener != null) {
                itemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    public QuizAdapter(List<Quiz> quizzes) {
        this.quizzes = quizzes;
    }

    @Override
    public QuizViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater vi = LayoutInflater.from(parent.getContext());
        View v = vi.inflate(R.layout.quiz_cell, parent, false);
        QuizViewHolder quizViewHolder = new QuizViewHolder(v);

        return quizViewHolder;
    }

    @Override
    public void onBindViewHolder(QuizViewHolder holder, int position) {
        Quiz quiz = quizzes.get(position);
        User user = Storage.getInstance().findUserById(quiz.getUserID());
        holder.tv_user_name.setText(user.getProfileName());
        holder.tv_question.setText(quiz.getQuestion());
        Picasso.with(holder.imgv_profile_picture.getContext()).load(user.getProfilePictureUri()).into(holder.imgv_profile_picture);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public int getItemCount() {
        return quizzes.size();
    }

//    public QuizAdapter(Context context, int resource, List<Quiz> objects) {
//        super(context, resource, objects);
//    }
//
//    public View getView(int position, View convertView, ViewGroup parent) {
//        View v = convertView;
//
//        if(v == null) {
//            LayoutInflater vi = LayoutInflater.from(getContext());
//            v = vi.inflate(R.layout.quiz_cell, null);
//        }
//
//        if(v != null) {
//            TextView tv_user_name = (TextView) v.findViewById(R.id.tv_user_name);
//            TextView tv_question = (TextView) v.findViewById(R.id.tv_question);
//            ImageView imgv_profile_picture = (ImageView) v.findViewById(R.id.imgv_profile_picture);
//            Quiz quiz = getItem(position);
//            User user = Storage.getInstance().findUserById(quiz.getUserID());
//            tv_user_name.setText(user.getProfileName());
//            tv_question.setText(quiz.getQuestion());
//            Picasso.with(getContext()).load(user.getProfilePictureUri()).into(imgv_profile_picture);
//        }
//
//        return v;
//    }
}
