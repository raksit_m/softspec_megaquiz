package com.example.softspec.megaquiz.models;

import java.io.Serializable;

/**
 * Created by Earth on 10/4/2559.
 */
public class User implements Serializable {
    private String userID;
    private String profileName;
    private String profilePictureUri;

    public User(String userID, String profileName, String profilePictureUri) {
        this.userID = userID;
        this.profileName = profileName;
        this.profilePictureUri = profilePictureUri;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public String getProfilePictureUri() {
        return profilePictureUri;
    }

    public void setProfilePictureUri(String profilePictureUri) {
        this.profilePictureUri = profilePictureUri;
    }
}
