package com.example.softspec.megaquiz.models;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 * Created by Earth on 5/4/2559.
 */
public class Storage extends Observable {
    private static Storage instance = null;
    private List<Quiz> quizzes;
    private List<User> users;
    private List<Answer> answers;

    private Storage() {
        quizzes = new ArrayList<Quiz>();
        users = new ArrayList<User>();
        answers = new ArrayList<Answer>();
    }

    public static Storage getInstance() {
        if (instance == null) instance = new Storage();
        return instance;
    }

    public String post(String url, List<NameValuePair> nameValuePairs) {
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);

        BufferedReader bufferedReader;

        String result = "";

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            try {
                HttpResponse httpResponse = httpClient.execute(httpPost);
                Log.e("Http Post Response: ", httpResponse.toString());

                InputStream inputStream = httpResponse.getEntity().getContent();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                String line = bufferedReader.readLine();

                result += line;

            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return result;
    }

    public String get(String url) {
        HttpClient httpClient = new DefaultHttpClient();

        HttpGet httpGet = new HttpGet(url);

        BufferedReader bufferedReader;

        String result = "";

        try {
            HttpResponse httpResponse = httpClient.execute(httpGet);
            Log.e("Http Get Response:", httpResponse.toString());

            InputStream inputStream = httpResponse.getEntity().getContent();

            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            String line = bufferedReader.readLine();

            result += line;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    public void addQuiz(String question, String choice_1, String choice_2, String choice_3, String choice_4, String userID) {

        String method = "adding_quiz";

        AsyncTask<String, Void, String> addingQuizTask = new AsyncTask<String, Void, String>() {

            @Override
            protected String doInBackground(String... params) {
                String method = params[0];
                Log.e("METHOD", method);

                String question = params[1];
                String choice_1 = params[2];
                String choice_2 = params[3];
                String choice_3 = params[4];
                String choice_4 = params[5];
                String userID = params[6];

                String url = "http://taweesoft.xyz/megaquiz/adding_quiz.php";

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(6);

                nameValuePairs.add(new BasicNameValuePair("question", question));
                nameValuePairs.add(new BasicNameValuePair("choice_1", choice_1));
                nameValuePairs.add(new BasicNameValuePair("choice_2", choice_2));
                nameValuePairs.add(new BasicNameValuePair("choice_3", choice_3));
                nameValuePairs.add(new BasicNameValuePair("choice_4", choice_4));
                nameValuePairs.add(new BasicNameValuePair("user_id", userID));

                String result = post(url, nameValuePairs);

                return result;
            }

            @Override
            protected void onPostExecute(String s) {
                Storage.getInstance().setChanged();
                Storage.getInstance().notifyObservers();
                super.onPostExecute(s);
            }
        }.execute(method, question, choice_1, choice_2, choice_3, choice_4, userID);
    }

    public void addUser(String userID, String userProfileName, String userProfilePictureUri) {

        String method = "adding_user";

        AsyncTask<String, Void, Void> addingUserTask = new AsyncTask<String, Void, Void>() {

            private User user;

            @Override
            protected Void doInBackground(String... params) {
                String method = params[0];

                Log.e("METHOD", method);

                String userID = params[1];
                String userProfileName = params[2];
                String userProfilePictureUri = params[3];

                String url = "http://taweesoft.xyz/megaquiz/adding_user.php";

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
                nameValuePairs.add(new BasicNameValuePair("id", userID));
                nameValuePairs.add(new BasicNameValuePair("profile_name", userProfileName));
                nameValuePairs.add(new BasicNameValuePair("profile_picture_uri", userProfilePictureUri));

                post(url, nameValuePairs);
                user = new User(userID, userProfileName, userProfilePictureUri);
                Storage.getInstance().getUsers().add(user);
                return null;

            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Storage.getInstance().setChanged();
                Storage.getInstance().notifyObservers(user);
            }
        }.execute(method, userID, userProfileName, userProfilePictureUri);
    }

    public void addAnswer(int quizID, int choiceID, String userID) {
        AsyncTask<String, Void, Void> addingAnswerTask = new AsyncTask<String, Void, Void>() {
            private int id;

            @Override
            protected Void doInBackground(String... params) {
                String quizID = params[0];
                String choiceID = params[1];
                String userID = params[2];

                id = Integer.parseInt(quizID);

                String url = "http://taweesoft.xyz/megaquiz/adding_answer.php";

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
                nameValuePairs.add(new BasicNameValuePair("quiz_id", quizID));
                nameValuePairs.add(new BasicNameValuePair("choice_id", choiceID));
                nameValuePairs.add(new BasicNameValuePair("user_id", userID));

                post(url, nameValuePairs);

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                getAnswersFromQuizID(id);
            }
        }.execute(Integer.toString(quizID), Integer.toString(choiceID), userID);
    }

    public void getAllData() {
        Storage.getInstance().getUsers().clear();
        Storage.getInstance().getQuizzes().clear();
        Storage.getInstance().getAnswers().clear();

        AsyncTask<Void, Void, Void> loadingTask = new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                String url = "http://taweesoft.xyz/megaquiz/loading_all_users.php";

                String result = Storage.this.get(url);

                try {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONArray jsonArray = jsonObject.getJSONArray("result");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        String userID = jo.getString("user_id");
                        String userProfileName = jo.getString("user_profile_name");
                        String userProfilePictureUri = jo.getString("user_profile_picture_uri");
                        User user = new User(userID, userProfileName, userProfilePictureUri);
                        Storage.getInstance().getUsers().add(user);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                url = "http://taweesoft.xyz/megaquiz/loading_all_quizzes.php";

                result = Storage.this.get(url);

                Log.e("RESULT", result);

                final int choicesAmount = 4;

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    JSONArray jsonArray = jsonObject.getJSONArray("result");

                    List<Choice> choicesFromQueries = new ArrayList<Choice>();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        int choice_id = jo.getInt("choice_id");
                        String choice_description = jo.getString("description");
                        choicesFromQueries.add(new Choice(choice_id, choice_description));
                    }

                    for (int i = 0; i < choicesFromQueries.size(); i += 4) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        int quiz_id = jo.getInt("quiz_id");
                        String user_id = jo.getString("user_id");
                        String quiz_question = jo.getString("question");

                        Choice[] choices = new Choice[choicesAmount];

                        for (int j = 0; j < choicesAmount; j++) {
                            choices[j] = choicesFromQueries.get(i + j);
                        }

                        Quiz quiz = new Quiz(quiz_id, quiz_question, choices, user_id);

                        Storage.getInstance().getQuizzes().add(quiz);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                url = "http://taweesoft.xyz/megaquiz/loading_all_answers.php";

                result = Storage.this.get(url);

                Log.e("RESULT", result);

                try {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONArray jsonArray = jsonObject.getJSONArray("result");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        int id = jo.getInt("answer_id");
                        int quiz_id = jo.getInt("quiz_id");
                        Quiz quiz = Storage.getInstance().findQuizById(quiz_id);
                        int choice_id = jo.getInt("choice_id");
                        Choice choice = Storage.getInstance().findChoiceById(quiz, choice_id);
                        String user_id = jo.getString("user_id");
                        Answer answer = new Answer(id, quiz, choice, user_id);
                        Storage.getInstance().getAnswers().add(answer);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Storage.this.setChanged();
                Storage.this.notifyObservers();
            }
        }.execute();
    }

    public void getAnswersFromQuizID(final int quizID) {
        AsyncTask<String, Void, Void> loadingAnswersFromQuizIDTask = new AsyncTask<String, Void, Void>() {

            private int[] numAnswersArray = new int[4];

            @Override
            protected Void doInBackground(String... params) {
                numAnswersArray = new int[4];
                String quizID = params[0];
                String url = "http://taweesoft.xyz/megaquiz/loading_answers_from_quiz_id.php";
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("quiz_id", quizID));

                String result = post(url, nameValuePairs);

                try {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONArray jsonArray = jsonObject.getJSONArray("result");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jo = jsonArray.getJSONObject(i);
                        int numAnswersForEachChoice = jo.getInt("num_answers");
                        numAnswersArray[i] = numAnswersForEachChoice;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Storage.this.setChanged();
                Storage.this.notifyObservers(numAnswersArray);
            }
        }.execute(Integer.toString(quizID));
    }

    public void getAnswerFromQuizAndUserID(final int quizID, final String userID) {
        AsyncTask<String, Void, Void> loadingAnswerFromQuizAndUserID = new AsyncTask<String, Void, Void>() {
            Answer answer = null;

            @Override
            protected Void doInBackground(String... params) {
                for(Answer a : Storage.getInstance().getAnswers()) {
                    if(a.getQuiz().getId() == quizID && a.getUserID().equals(userID)) {
                        answer = a;
                        return null;
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Storage.this.setChanged();
                Storage.this.notifyObservers(answer);
            }
        }.execute(Integer.toString(quizID), userID);
    }

    public int getQuizCreatedTimesFromUserID(String userID) {
        int sum = 0;
        for (Quiz q : Storage.getInstance().getQuizzes()) {
            if (q.getUserID().equals(userID)) {
                sum++;
            }
        }
        return sum;
    }

    public int getAnswerTimesFromUserID(String userID) {
        int sum = 0;
        for (Answer a : Storage.getInstance().getAnswers()) {
            if (a.getUserID().equals(userID)) {
                sum++;
            }
        }
        return sum;
    }

    public Quiz findQuizById(int quizID) {
        for (int i = 0; i < quizzes.size(); i++) {
            if (quizID == (quizzes.get(i).getId())) {
                return quizzes.get(i);
            }
        }
        return null;
    }

    public Choice findChoiceById(Quiz quiz, int choiceID) {
        for (int i = 0; i < quiz.getChoices().length; i++) {
            if (choiceID == quiz.getChoices()[i].getId()) {
                return quiz.getChoices()[i];
            }
        }
        return null;
    }

    public User findUserById(String userID) {
        for (int i = 0; i < users.size(); i++) {
            if (userID.equals(users.get(i).getUserID())) {
                return users.get(i);
            }
        }
        return null;
    }

    public List<Quiz> getQuizzes() {
        return quizzes;
    }

    public List<User> getUsers() {
        return users;
    }

    public List<Answer> getAnswers() {
        return answers;
    }
}