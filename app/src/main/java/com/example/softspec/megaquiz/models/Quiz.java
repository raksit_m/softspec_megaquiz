package com.example.softspec.megaquiz.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Earth on 5/4/2559.
 */
public class Quiz implements Serializable {
    private int id;
    private String question;
    private Choice[] choices;
    private String userID;

    public Quiz(int id, String question, Choice[] choices, String userID) {
        this.id = id;
        this.question = question;
        this.choices = choices;
        this.userID = userID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Choice[] getChoices() {
        return choices;
    }

    public void setChoices(Choice[] choices) {
        this.choices = choices;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }
}
