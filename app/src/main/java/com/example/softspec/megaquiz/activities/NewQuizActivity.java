package com.example.softspec.megaquiz.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Toast;

import com.example.softspec.megaquiz.R;
import com.example.softspec.megaquiz.models.Storage;
import com.example.softspec.megaquiz.models.User;

import java.util.Observable;
import java.util.Observer;

public class NewQuizActivity extends AppCompatActivity implements Observer {

    public static final int NUM_FIELDS = 5;
    private EditText edtxt_question;
    private EditText edtxt_choice_1;
    private EditText edtxt_choice_2;
    private EditText edtxt_choice_3;
    private EditText edtxt_choice_4;
    private EditText[] editTexts;
    private Button btn_done;
    private String userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_quiz);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initComponents();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Storage.getInstance().deleteObservers();
        Storage.getInstance().addObserver(this);
    }

    private void initComponents() {
        edtxt_question = (EditText) findViewById(R.id.edtxt_question);
        edtxt_choice_1 = (EditText) findViewById(R.id.edtxt_choice1);
        edtxt_choice_2 = (EditText) findViewById(R.id.edtxt_choice2);
        edtxt_choice_3 = (EditText) findViewById(R.id.edtxt_choice3);
        edtxt_choice_4 = (EditText) findViewById(R.id.edtxt_choice4);

        editTexts = new EditText[5];
        editTexts[0] = edtxt_question;
        editTexts[1] = edtxt_choice_1;
        editTexts[2] = edtxt_choice_2;
        editTexts[3] = edtxt_choice_3;
        editTexts[4] = edtxt_choice_4;

        userID = getIntent().getStringExtra("user_id");
        btn_done = (Button) findViewById(R.id.btn_done);

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String question = edtxt_question.getText().toString();
                String choice_1 = edtxt_choice_1.getText().toString();
                String choice_2 = edtxt_choice_2.getText().toString();
                String choice_3 = edtxt_choice_3.getText().toString();
                String choice_4 = edtxt_choice_4.getText().toString();

                String[] fields = new String[NUM_FIELDS];
                fields[0] = question;
                fields[1] = choice_1;
                fields[2] = choice_2;
                fields[3] = choice_3;
                fields[4] = choice_4;

                for(int i = 0; i < editTexts.length; i++) {
                    if(fields[i].isEmpty()) {
                        editTexts[i].setError("Please enter this field.");
                        Toast.makeText(NewQuizActivity.this, "Please complete the form", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                Storage.getInstance().addQuiz(question, choice_1, choice_2,choice_3, choice_4, userID);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == android.R.id.home) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Discard draft?");
            builder.setCancelable(true);

            builder.setPositiveButton("Discard", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    NewQuizActivity.this.finish();
                }
            });

            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            AlertDialog alertDialog = builder.create();
            alertDialog.show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void update(Observable observable, Object data) {
        Intent intent = new Intent(NewQuizActivity.this, MyQuizActivity.class);
        User user = Storage.getInstance().findUserById(userID);
        intent.putExtra("user_id", userID);
        intent.putExtra("user_profile_name", user.getProfileName());
        intent.putExtra("user_profile_picture_uri", user.getProfilePictureUri());
        startActivity(intent);
        finish();
    }
}
