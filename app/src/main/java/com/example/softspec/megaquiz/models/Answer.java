package com.example.softspec.megaquiz.models;

import java.io.Serializable;

/**
 * Created by Earth on 11/4/2559.
 */
public class Answer implements Serializable {
    private int id;
    private Quiz quiz;
    private Choice choice;
    private String userID;

    public Answer(int id, Quiz quiz, Choice choice, String userID) {
        this.id = id;
        this.quiz = quiz;
        this.choice = choice;
        this.userID = userID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

    public Choice getChoice() {
        return choice;
    }

    public void setChoice(Choice choice) {
        this.choice = choice;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }
}
