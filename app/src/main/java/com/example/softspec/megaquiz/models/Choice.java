package com.example.softspec.megaquiz.models;

import java.io.Serializable;

/**
 * Created by Earth on 5/4/2559.
 */
public class Choice implements Serializable {
    private int id;
    private String description;

    public Choice(int id, String description) {
        this.id = id;
        this.description = description;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
